package pl.michalkarwowski.player_stat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.michalkarwowski.App;
import pl.michalkarwowski.connector.ApiConnector;
import pl.michalkarwowski.language.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

public class PlayerStatPanel extends JPanel implements ActionListener {

    private ApiConnector apiConnector;
    private Language language;

    private JLabel playerStatisticsLabel;
    private JLabel playerNameLabel;
    private JLabel roundsPlayedLabel;
    private JLabel roundsPlayedValueLabel;
    private JLabel winRoundsLabel;
    private JLabel winRoundsValueLabel;
    private JLabel bestScoreInOneLabel;
    private JLabel bestScoreInOneValueLabel;
    private JLabel roundsLostLabel;
    private JLabel roundsLostValueLabel;


    private JButton refreshButton;

    private JTextField enterPlayerNameTextField;
    private JLabel playerTotalScoreValueLabel;
    private JLabel computerTotalScoreValueLabel;
    private JLabel playerTotalScoreLabel;
    private JLabel computerTotalScoreLabel;

    private static final Logger LOG = LoggerFactory.getLogger(PlayerStatPanel.class);


    public PlayerStatPanel(ApiConnector apiConnector, Language language) {
        this.apiConnector = apiConnector;
        this.language = language;
        initUI();
        setMinimumSize(new Dimension(50, 100));
    }

    private void initUI() {
        setLayout(new GridBagLayout());

        GridBagConstraints gbc;

        playerStatisticsLabel = new JLabel();
        playerStatisticsLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        playerStatisticsLabel.setText(language.getPlayerStatistics());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 3;
        gbc.weightx = 1.0;
        add(playerStatisticsLabel, gbc);

        playerNameLabel = new JLabel();
        playerNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
//        playerNameLabel.setBorder(BorderFactory.createLineBorder(Color.RED));
        playerNameLabel.setPreferredSize(new Dimension(150, 20));
        playerNameLabel.setText(language.getPlayerName());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(playerNameLabel, gbc);

        enterPlayerNameTextField = new JTextField();
        enterPlayerNameTextField.setText(language.getPlayerNameVal());
        enterPlayerNameTextField.setPreferredSize(new Dimension(125, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        add(enterPlayerNameTextField, gbc);

        refreshButton = new JButton();
        refreshButton.setText(language.getRefreshButton());
        refreshButton.addActionListener(this);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 3;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(refreshButton, gbc);

        final JPanel spacer = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 3;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.ipady = 20;
        add(spacer, gbc);

        roundsPlayedLabel = new JLabel();
        roundsPlayedLabel.setHorizontalAlignment(SwingConstants.CENTER);
        roundsPlayedLabel.setPreferredSize(new Dimension(150, 20));
        roundsPlayedLabel.setText(language.getRoundPlayed());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(roundsPlayedLabel, gbc);

        roundsPlayedValueLabel = new JLabel();
        roundsPlayedValueLabel.setText("0");
        roundsPlayedValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        add(roundsPlayedValueLabel, gbc);

        winRoundsLabel = new JLabel();
        winRoundsLabel.setHorizontalAlignment(SwingConstants.CENTER);
//        winRoundsLabel.setBorder(BorderFactory.createLineBorder(Color.RED));
        winRoundsLabel.setPreferredSize(new Dimension(150, 20));
        winRoundsLabel.setText(language.getWinRounds());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(winRoundsLabel, gbc);

        winRoundsValueLabel = new JLabel();
        winRoundsValueLabel.setText("0");
        winRoundsValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        add(winRoundsValueLabel, gbc);

        roundsLostLabel = new JLabel();
        roundsLostLabel.setHorizontalAlignment(SwingConstants.CENTER);
        roundsLostLabel.setPreferredSize(new Dimension(150, 20));
        roundsLostLabel.setText(language.getLostRounds());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(roundsLostLabel, gbc);

        roundsLostValueLabel = new JLabel();
        roundsLostValueLabel.setText("0");
        roundsLostValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 6;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        add(roundsLostValueLabel, gbc);

        bestScoreInOneLabel = new JLabel();
        bestScoreInOneLabel.setHorizontalAlignment(SwingConstants.CENTER);
//        bestScoreInOneLabel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        bestScoreInOneLabel.setPreferredSize(new Dimension(150, 20));
        bestScoreInOneLabel.setText(language.getBestScore());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(bestScoreInOneLabel, gbc);

        bestScoreInOneValueLabel = new JLabel();
        bestScoreInOneValueLabel.setText("0");
        bestScoreInOneValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        add(bestScoreInOneValueLabel, gbc);

        playerTotalScoreLabel = new JLabel();
        playerTotalScoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        playerTotalScoreLabel.setText(language.getPlayerTotalScore());
        playerTotalScoreLabel.setPreferredSize(new Dimension(150, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(playerTotalScoreLabel, gbc);

        playerTotalScoreValueLabel = new JLabel();
        playerTotalScoreValueLabel.setText("0");
        playerTotalScoreValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        add(playerTotalScoreValueLabel, gbc);

        computerTotalScoreLabel = new JLabel();
        computerTotalScoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        computerTotalScoreLabel.setText(language.getComputerTotalScore());
        computerTotalScoreLabel.setPreferredSize(new Dimension(175, 20));
//        computerTotalScoreLabel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(computerTotalScoreLabel, gbc);

        computerTotalScoreValueLabel = new JLabel();
        computerTotalScoreValueLabel.setText("0");
//        computerTotalScoreValueLabel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        computerTotalScoreValueLabel.setPreferredSize(new Dimension(50, 20));
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        add(computerTotalScoreValueLabel, gbc);

    }

    public void actionPerformed(ActionEvent e) {

        SwingWorker<PlayerStat, Void> worker = new SwingWorker<PlayerStat, Void>() {
            @Override
            protected PlayerStat doInBackground() throws Exception {
                return apiConnector.getResponse(enterPlayerNameTextField.getText());
            }

            @Override
            protected void done() {
                try {
                    roundsPlayedValueLabel.setText(Integer.toString(get().getRoundsPlayed()));
                    winRoundsValueLabel.setText(Integer.toString(get().getWinRounds()));
                    roundsLostValueLabel.setText(Integer.toString(get().getLostRounds()));
                    bestScoreInOneValueLabel.setText((Integer.toString(get().getBestScoreInOneRound())));
                    playerTotalScoreValueLabel.setText(Long.toString(get().getPlayerTotalScore()));
                    computerTotalScoreValueLabel.setText(Long.toString(get().getComputerTotalScore()));
                } catch (InterruptedException e1) {
                    System.out.println("Error: " + e1);
                    LOG.error("Error: " + e1);
                } catch (ExecutionException e1) {
                    LOG.error("Player not found");
                    JOptionPane.showMessageDialog(getRootPane(),
                            language.getPlayerNotFoundMessage(),
                            language.getPlayerNotFound(),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        };

        worker.execute();
    }
}
