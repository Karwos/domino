package pl.michalkarwowski.player_stat;

public class PlayerStat {

    private int roundsPlayed;
    private int winRounds;
    private int lostRounds;
    private int bestScoreInOneRound;
    private long playerTotalScore;
    private long computerTotalScore;

    public int getRoundsPlayed() {
        return roundsPlayed;
    }

    public void setRoundsPlayed(int roundsPlayed) {
        this.roundsPlayed = roundsPlayed;
    }

    public int getWinRounds() {
        return winRounds;
    }

    public void setWinRounds(int winRounds) {
        this.winRounds = winRounds;
    }

    public int getLostRounds() {
        return lostRounds;
    }

    public void setLostRounds(int lostRounds) {
        this.lostRounds = lostRounds;
    }

    public int getBestScoreInOneRound() {
        return bestScoreInOneRound;
    }

    public void setBestScoreInOneRound(int bestScoreInOneRound) {
        this.bestScoreInOneRound = bestScoreInOneRound;
    }

    public long getPlayerTotalScore() {
        return playerTotalScore;
    }

    public void setPlayerTotalScore(long playerTotalScore) {
        this.playerTotalScore = playerTotalScore;
    }

    public long getComputerTotalScore() {
        return computerTotalScore;
    }

    public void setComputerTotalScore(long computerTotalScore) {
        this.computerTotalScore = computerTotalScore;
    }
}
