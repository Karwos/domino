package pl.michalkarwowski;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.michalkarwowski.connector.ApiConnector;
import pl.michalkarwowski.connector.DataBaseConnector;
import pl.michalkarwowski.game.GamePanel;
import pl.michalkarwowski.language.Language;
import pl.michalkarwowski.player_stat.PlayerStat;
import pl.michalkarwowski.player_stat.PlayerStatPanel;

import javax.swing.*;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class Window extends JFrame implements ActionListener {

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem newGame;
    private JMenuItem saveGame;
    private JMenuItem loadGame;
    private JMenu skinSumMenu;
    private JMenuItem skinItem1;
    private JMenuItem skinItem2;
    private JMenuItem skinItem3;
    private GamePanel gamePanel;
    private PlayerStatPanel playerStatPanel;
    private Language language;
    private String userName;
    private DataBaseConnector dataBaseConnector;
    private Properties properties;
    private static final Logger LOG = LoggerFactory.getLogger(Window.class);
//    private LoginPanel loginPanel;

    public Window(Properties properties) {
        this.properties = properties;
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Language.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            language = (Language) jaxbUnmarshaller.unmarshal(new File(properties.getProperty("language")));
        } catch (JAXBException e) {
            System.out.println("Error: " + e);
        }
        dataBaseConnector = new DataBaseConnector(properties);
        //loginPanel = new LoginPanel(this, language);


        initUI();
    }

    private void initUI() {

        JPanel mainPanel = new JPanel();

        menuBar = new JMenuBar();
        fileMenu = new JMenu(language.getFile());

        newGame = new JMenuItem(language.getNewGame());
        newGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, Event.CTRL_MASK));
        newGame.addActionListener(this);

        saveGame = new JMenuItem(language.getSaveGame());
        saveGame.addActionListener(this);
        loadGame = new JMenuItem(language.getLoadGame());
        loadGame.addActionListener(this);

        skinSumMenu = new JMenu("PLAF");

        skinItem1 = new JMenuItem(language.getSkin1());
        skinItem1.addActionListener(this);

        skinItem2 = new JMenuItem(language.getSkin2());
        skinItem2.addActionListener(this);

        skinItem3 = new JMenuItem(language.getSkin3());
        skinItem3.addActionListener(this);

        skinSumMenu.add(skinItem1);
        skinSumMenu.add(skinItem2);
        skinSumMenu.add(skinItem3);

        fileMenu.add(newGame);

        fileMenu.add(skinSumMenu);

        fileMenu.add(saveGame);
        fileMenu.add(loadGame);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        gamePanel = new GamePanel(language, userName, dataBaseConnector);
        gamePanel.setPreferredSize(new Dimension(1000, 600));

        playerStatPanel = new PlayerStatPanel(new ApiConnector(properties.getProperty("endpoint")), language);

        mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 6;
        gbc.fill = GridBagConstraints.CENTER;
        mainPanel.add(gamePanel, gbc);


        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 0;
        gbc.gridheight = 0;
        gbc.anchor = GridBagConstraints.EAST;
        mainPanel.add(playerStatPanel, gbc);

        add(mainPanel, BorderLayout.CENTER);

        pack();
        //setSize(1366, 740);
        setTitle(language.getTitle());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
    public DataBaseConnector getDataBaseConnector() {
        return dataBaseConnector;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setUser(String user) {
        this.userName = user;
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == newGame) {
            gamePanel.newGame();
        } else if (e.getSource() == skinItem1) {
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                MetalLookAndFeel.setCurrentTheme(new OceanTheme());
                UIManager.setLookAndFeel(new MetalLookAndFeel());
                SwingUtilities.updateComponentTreeUI(this);
                LOG.info("PLAF changed to skin 1");
            } catch (Exception exception) {
                LOG.error("Error changing PLAF to skin 1");
                JOptionPane.showMessageDialog(this, "Can't change look and feel", "Invalid PLAF",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == skinItem2) {
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                MetalLookAndFeel.setCurrentTheme(new DefaultMetalTheme());
                UIManager.setLookAndFeel(new MetalLookAndFeel());
                SwingUtilities.updateComponentTreeUI(this);
                LOG.info("PLAF changed to skin 2");
            } catch (Exception exception) {
                LOG.error("Error changing PLAF to skin 1");
                JOptionPane.showMessageDialog(this, "Can't change look and feel", "Invalid PLAF",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == skinItem3) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
                LOG.info("PLAF changed to skin 3");
            } catch (Exception exception) {
                LOG.error("Error changing PLAF to skin 1");
                JOptionPane.showMessageDialog(this, "Can't change look and feel", "Invalid PLAF",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == saveGame){
            gamePanel.saveGame();
        } else if(e.getSource() == loadGame)
            gamePanel.loadGame();
    }
}
