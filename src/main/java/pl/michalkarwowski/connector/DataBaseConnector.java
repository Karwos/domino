package pl.michalkarwowski.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.michalkarwowski.App;

import java.sql.*;
import java.util.Properties;

public class DataBaseConnector {
    private Properties properties;
    private static final Logger LOG = LoggerFactory.getLogger(DataBaseConnector.class);

    public DataBaseConnector(Properties properties) {
        this.properties = properties;
    }

    public Connection connectToDB() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(properties.getProperty("url"),
                    properties.getProperty("user"),
                    properties.getProperty("password"));

            LOG.info("Connected to DB");
        } catch (SQLException e) {
            System.out.println("Error: " + e);
            LOG.error("DataBaseConnector Error: " + e);
        }
        return connection;
    }

    public void saveScore(String userName, int computerPoints, int playerPoints, boolean isWin) {
        String sql = "insert into Games(user, computer_Score, player_Score, is_Win) values ('" +
                userName +
                "'," + computerPoints +
                "," + playerPoints +
                "," + isWin + ")";
        Connection connection = connectToDB();
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            LOG.info("Score saved in DataBase");

        } catch (SQLException e) {
            System.out.println("Error: " + e);
            LOG.error("DataBaseConnector Error: " + e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Error: " + e);
                LOG.error("DataBaseConnector Error: " + e);
            }
        }
    }
}
