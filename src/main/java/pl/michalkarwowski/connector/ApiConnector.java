package pl.michalkarwowski.connector;

import com.google.gson.Gson;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.michalkarwowski.player_stat.PlayerStat;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class ApiConnector {

    private String endPoint;
    private static final Logger LOG = LoggerFactory.getLogger(ApiConnector.class);

    public ApiConnector(String endPoint) {
        this.endPoint = endPoint;
    }

    public PlayerStat getResponse(String playerName) throws PlayerNotFoundException {
        PlayerStat playerStat = null;
        try {
            URLConnection connection = new URL(endPoint + "/" + playerName.replaceAll("\\s+", "+")).openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            int responseCode = httpConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream in = httpConnection.getInputStream();

                BufferedReader rd = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);

                JSONObject jsonArray = new JSONObject(jsonText);

                Gson gson = new Gson();

                playerStat = gson.fromJson(String.valueOf(jsonArray), PlayerStat.class);
            } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                throw new PlayerNotFoundException();
            } else if (responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                throw new PlayerNotFoundException();
            }
            if(playerStat == null)
                throw new PlayerNotFoundException();
        } catch (MalformedURLException e) {
            System.out.println("Error: " + e);
            LOG.error("APIConnector Error: " + e);
        } catch (IOException e) {
            System.out.println("Error: " + e);
            LOG.error("APIConnector Error: " + e);
        }
        return playerStat;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}
