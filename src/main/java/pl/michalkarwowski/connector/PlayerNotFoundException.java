package pl.michalkarwowski.connector;

public class PlayerNotFoundException extends Exception {
    // Parameterless Constructor
    public PlayerNotFoundException() {}

    // Constructor that accepts a message
    public PlayerNotFoundException(String message)
    {
        super(message);
    }
}