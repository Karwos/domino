package pl.michalkarwowski;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;



public class App {

    private final static String propertiesPath = "src/main/resources/app.properties";
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {

        FileInputStream in = null;
        try {
            in = new FileInputStream(propertiesPath);
            final Properties properties = new Properties();
            properties.load(in);

            LOG.info("Properties loaded");

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    Window window = new Window(properties);
                }
            });

        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e);
        } catch (IOException e) {
            System.out.println("Error: " + e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
        }



    }
}
