package pl.michalkarwowski.language;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Language {

    @XmlElement
    private String title;
    @XmlElement
    private String file;
    @XmlElement
    private String newGame;
    @XmlElement
    private String skin1;
    @XmlElement
    private String skin2;
    @XmlElement
    private String skin3;
    @XmlElement
    private String saveGame;
    @XmlElement
    private String loadGame;
    @XmlElement
    private String optionPanel;
    @XmlElement
    private String optionPanelMessage;
    @XmlElement
    private String optionPanelLeft;
    @XmlElement
    private String optionPanelRight;
    @XmlElement
    private String noMoreMove;
    @XmlElement
    private String noMoreMoveMessage;
    @XmlElement
    private String gameOver;
    @XmlElement
    private String gameOverWinMessage;
    @XmlElement
    private String gameOverLoseMessage;
    @XmlElement
    private String gameOverPlayerScore;
    @XmlElement
    private String gameOverComputerScore;
    @XmlElement
    private String playerStatistics;
    @XmlElement
    private String playerName;
    @XmlElement
    private String playerNameVal;
    @XmlElement
    private String refreshButton;
    @XmlElement
    private String roundPlayed;
    @XmlElement
    private String winRounds;
    @XmlElement
    private String lostRounds;
    @XmlElement
    private String bestScore;
    @XmlElement
    private String playerNotFound;
    @XmlElement
    private String playerNotFoundMessage;
    @XmlElement
    private String loginPanel;
    @XmlElement
    private String login;
    @XmlElement
    private String password;
    @XmlElement
    private String loginButton;
    @XmlElement
    private String exitButton;
    @XmlElement
    private String loginError;
    @XmlElement
    private String loginErrorMessage;
    @XmlElement
    private String inputPlayerName;
    @XmlElement
    private String inputPlayerNameMessage;
    @XmlElement
    private String playerTotalScore;
    @XmlElement
    private String computerTotalScore;

    public Language() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getNewGame() {
        return newGame;
    }

    public void setNewGame(String newGame) {
        this.newGame = newGame;
    }

    public String getSkin1() {
        return skin1;
    }

    public void setSkin1(String skin1) {
        this.skin1 = skin1;
    }

    public String getSkin2() {
        return skin2;
    }

    public void setSkin2(String skin2) {
        this.skin2 = skin2;
    }

    public String getSkin3() {
        return skin3;
    }

    public void setSkin3(String skin3) {
        this.skin3 = skin3;
    }

    public String getSaveGame() {
        return saveGame;
    }

    public void setSaveGame(String saveGame) {
        this.saveGame = saveGame;
    }

    public String getLoadGame() {
        return loadGame;
    }

    public void setLoadGame(String loadGame) {
        this.loadGame = loadGame;
    }

    public String getOptionPanel() {
        return optionPanel;
    }

    public void setOptionPanel(String optionPanel) {
        this.optionPanel = optionPanel;
    }

    public String getOptionPanelMessage() {
        return optionPanelMessage;
    }

    public void setOptionPanelMessage(String optionPanelMessage) {
        this.optionPanelMessage = optionPanelMessage;
    }

    public String getOptionPanelLeft() {
        return optionPanelLeft;
    }

    public void setOptionPanelLeft(String optionPanelLeft) {
        this.optionPanelLeft = optionPanelLeft;
    }

    public String getOptionPanelRight() {
        return optionPanelRight;
    }

    public void setOptionPanelRight(String optionPanelRight) {
        this.optionPanelRight = optionPanelRight;
    }

    public String getNoMoreMove() {
        return noMoreMove;
    }

    public void setNoMoreMove(String noMoreMove) {
        this.noMoreMove = noMoreMove;
    }

    public String getNoMoreMoveMessage() {
        return noMoreMoveMessage;
    }

    public void setNoMoreMoveMessage(String noMoreMoveMessage) {
        this.noMoreMoveMessage = noMoreMoveMessage;
    }

    public String getGameOver() {
        return gameOver;
    }

    public void setGameOver(String gameOver) {
        this.gameOver = gameOver;
    }

    public String getGameOverWinMessage() {
        return gameOverWinMessage;
    }

    public void setGameOverWinMessage(String gameOverWinMessage) {
        this.gameOverWinMessage = gameOverWinMessage;
    }

    public String getGameOverLoseMessage() {
        return gameOverLoseMessage;
    }

    public void setGameOverLoseMessage(String gameOverLoseMessage) {
        this.gameOverLoseMessage = gameOverLoseMessage;
    }

    public String getGameOverPlayerScore() {
        return gameOverPlayerScore;
    }

    public void setGameOverPlayerScore(String gameOverPlayerScore) {
        this.gameOverPlayerScore = gameOverPlayerScore;
    }

    public String getGameOverComputerScore() {
        return gameOverComputerScore;
    }

    public void setGameOverComputerScore(String gameOverComputerScore) {
        this.gameOverComputerScore = gameOverComputerScore;
    }

    public String getPlayerStatistics() {
        return playerStatistics;
    }

    public void setPlayerStatistics(String playerStatistics) {
        this.playerStatistics = playerStatistics;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerNameVal() {
        return playerNameVal;
    }

    public void setPlayerNameVal(String playerNameVal) {
        this.playerNameVal = playerNameVal;
    }

    public String getRefreshButton() {
        return refreshButton;
    }

    public void setRefreshButton(String refreshButton) {
        this.refreshButton = refreshButton;
    }

    public String getRoundPlayed() {
        return roundPlayed;
    }

    public void setRoundPlayed(String roundPlayed) {
        this.roundPlayed = roundPlayed;
    }

    public String getWinRounds() {
        return winRounds;
    }

    public void setWinRounds(String winRounds) {
        this.winRounds = winRounds;
    }

    public String getLostRounds() {
        return lostRounds;
    }

    public void setLostRounds(String lostRounds) {
        this.lostRounds = lostRounds;
    }

    public String getBestScore() {
        return bestScore;
    }

    public void setBestScore(String bestScore) {
        this.bestScore = bestScore;
    }

    public String getPlayerNotFoundMessage() {
        return playerNotFoundMessage;
    }

    public void setPlayerNotFoundMessage(String playerNotFoundMessage) {
        this.playerNotFoundMessage = playerNotFoundMessage;
    }

    public String getPlayerNotFound() {
        return playerNotFound;
    }

    public void setPlayerNotFound(String playerNotFound) {
        this.playerNotFound = playerNotFound;
    }

    public String getLoginPanel() {
        return loginPanel;
    }

    public void setLoginPanel(String loginPanel) {
        this.loginPanel = loginPanel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(String loginButton) {
        this.loginButton = loginButton;
    }

    public String getExitButton() {
        return exitButton;
    }

    public void setExitButton(String exitButton) {
        this.exitButton = exitButton;
    }

    public String getLoginError() {
        return loginError;
    }

    public void setLoginError(String loginError) {
        this.loginError = loginError;
    }

    public String getLoginErrorMessage() {
        return loginErrorMessage;
    }

    public void setLoginErrorMessage(String loginErrorMessage) {
        this.loginErrorMessage = loginErrorMessage;
    }

    public String getInputPlayerName() {
        return inputPlayerName;
    }

    public void setInputPlayerName(String inputPlayerName) {
        this.inputPlayerName = inputPlayerName;
    }

    public String getInputPlayerNameMessage() {
        return inputPlayerNameMessage;
    }

    public void setInputPlayerNameMessage(String inputPlayerNameMessage) {
        this.inputPlayerNameMessage = inputPlayerNameMessage;
    }

    public String getPlayerTotalScore() {
        return playerTotalScore;
    }

    public void setPlayerTotalScore(String playerTotalScore) {
        this.playerTotalScore = playerTotalScore;
    }

    public String getComputerTotalScore() {
        return computerTotalScore;
    }

    public void setComputerTotalScore(String computerTotalScore) {
        this.computerTotalScore = computerTotalScore;
    }
}


