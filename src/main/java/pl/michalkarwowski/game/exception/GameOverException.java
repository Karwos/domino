package pl.michalkarwowski.game.exception;

public class GameOverException extends Exception {
    // Parameterless Constructor
    public GameOverException() {
    }

    // Constructor that accepts a message
    public GameOverException(String message) {
        super(message);
    }
}