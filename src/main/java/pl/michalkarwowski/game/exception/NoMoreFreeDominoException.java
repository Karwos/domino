package pl.michalkarwowski.game.exception;

public class NoMoreFreeDominoException extends Exception {
    // Parameterless Constructor
    public NoMoreFreeDominoException() {
    }

    // Constructor that accepts a message
    public NoMoreFreeDominoException(String message) {
        super(message);
    }
}