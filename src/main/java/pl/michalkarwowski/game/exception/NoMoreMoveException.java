package pl.michalkarwowski.game.exception;

public class NoMoreMoveException extends Exception {
    // Parameterless Constructor
    public NoMoreMoveException() {}

    // Constructor that accepts a message
    public NoMoreMoveException(String message)
    {
        super(message);
    }
}
