package pl.michalkarwowski.game.exception;

public class AdditionOnTwoSidesException extends Exception {
    // Parameterless Constructor
    public AdditionOnTwoSidesException() {}

    // Constructor that accepts a message
    public AdditionOnTwoSidesException(String message)
    {
        super(message);
    }
}
