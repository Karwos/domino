package pl.michalkarwowski.game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class DominoTile {

    private int number1;
    private int number2;
    private int x;
    private int y;
    private boolean isFaceUp;

    private transient Image tileImage;
    private boolean isDouble;

    public DominoTile(int number1, int number2, boolean isFaceUp) {
        this.number1 = number1;
        this.number2 = number2;
        this.isFaceUp = isFaceUp;
        if (number1 == number2) {
            isDouble = true;
        }

        loadImage();
    }

    public void flipDominoFace(boolean isGameOver) {
        if (isFaceUp()) {
            setFaceUp(false);
            try {
                tileImage = ImageIO.read(new File("src/main/resources/static/img/flipped.png"));
                tileImage = tileImage.getScaledInstance(80, 40, 1);
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
        } else {
            setFaceUp(true);
            if(isDouble && !isGameOver){
                loadVerticalImage();
            } else {
                loadImage();
            }
        }

    }

    public void flip() {

        int tmp = number2;
        number2 = number1;
        number1 = tmp;

        loadImage();
    }

    public void loadImage() {
        try {
            tileImage = ImageIO.read(new File("src/main/resources/static/img/" + number1 + "_" + number2 + ".png"));
            tileImage = tileImage.getScaledInstance(80, 40, 1);
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
    }

    public void loadVerticalImage() {
        if (isDouble) {
            try {
                tileImage = ImageIO.read(new File("src/main/resources/static/img/" + number1 + "_" + number2 + "_v.png"));
                tileImage = tileImage.getScaledInstance(40, 80, 1);
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
        }
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public Image getTileImage() {
        return tileImage;
    }

    public void setTileImage(Image tileImage) {
        this.tileImage = tileImage;
    }

    public boolean isFaceUp() {
        return isFaceUp;
    }

    public void setFaceUp(boolean faceUp) {
        isFaceUp = faceUp;
    }

    public boolean isDouble() {
        return isDouble;
    }

    @Override
    public String toString() {
        return "DominoTile{" +
                "number1=" + number1 +
                ", number2=" + number2 +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
