package pl.michalkarwowski.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.michalkarwowski.App;
import pl.michalkarwowski.GameState;
import pl.michalkarwowski.connector.DataBaseConnector;
import pl.michalkarwowski.game.exception.AdditionOnTwoSidesException;
import pl.michalkarwowski.game.exception.GameOverException;
import pl.michalkarwowski.game.exception.NoMoreFreeDominoException;
import pl.michalkarwowski.game.exception.NoMoreMoveException;
import pl.michalkarwowski.language.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class Board extends JPanel implements ActionListener {

    private ArrayList<DominoTile> computerDominoSet = new ArrayList<DominoTile>();   //computers domino tiles
    private ArrayList<DominoTile> freeDominoSet = new ArrayList<DominoTile>();       //free domino tiles on board
    private ArrayList<TileButton> playerDominoButtons = new ArrayList<TileButton>();

    private LinkedList<DominoTile> onBoardDomino = new LinkedList<DominoTile>();      //domino on board

    private Random generator = new Random();

    private int xDominoStart = 25;
    private int yPlayerDomino = 500;
    private int yComputerDomino = 25;
    private int yDominoOnBoard = 300;
    private int widthDomino = 80;
    private int heightDomino = 40;
    private int spaceDomino = 5;

    private int computerExtraPoints = 0;
    private int playerExtraPoints = 0;
    private int computerPoints = 0;
    private int playerPoints = 0;

    private boolean isWin = false;

    private Language language;

    private String playerName;

    private DataBaseConnector dataBaseConnector;

    private static final Logger LOG = LoggerFactory.getLogger(Board.class);

    public Board(Language language, String user, DataBaseConnector dataBaseConnector) {

        this.language = language;
        this.playerName = user;
        this.dataBaseConnector = dataBaseConnector;
        initBoard();
    }

    public void newGame() {
        while (playerDominoButtons.size() != 0) {
            TileButton button = playerDominoButtons.get(0);
            playerDominoButtons.remove(button);
            Container parent = button.getParent();     //remove button from player domino set
            parent.remove(button);
        }
        freeDominoSet = new ArrayList<DominoTile>();
        playerDominoButtons = new ArrayList<TileButton>();
        computerDominoSet = new ArrayList<DominoTile>();
        onBoardDomino = new LinkedList<DominoTile>();
        isWin = false;
        playerExtraPoints = 0;
        playerPoints = 0;
        computerExtraPoints = 0;
        computerPoints = 0;
        initBoard();
        repaint();

        LOG.info("New Game");
    }

    private void initBoard() {

        setLayout(null);

        setDominoTiles();

        setBackground(new Color(0, 145, 0));
        for (JButton button : playerDominoButtons) {
            button.addActionListener(this);
            add(button);
        }

    }

    private void setDominoTiles() {
        for (int i = 0; i <= 6; i++)            //Generate all domino
            for (int j = 0; j <= i; j++) {
                freeDominoSet.add(new DominoTile(i, j, true));
            }

        for (int i = 0; i < 7; i++) {           //losuje poczatkowe kostki dla komputera i gracza
            int tileNumber = generator.nextInt(freeDominoSet.size());
            computerDominoSet.add(freeDominoSet.remove(tileNumber));

            tileNumber = generator.nextInt(freeDominoSet.size());
            playerDominoButtons.add(new TileButton(freeDominoSet.remove(tileNumber)));

            computerDominoSet.get(i).flipDominoFace(false);      //odwraca kostki komputera
        }

        setDominoPosition();
    }

    private void addDominoPlayer() throws NoMoreFreeDominoException {
        if (freeDominoSet.size() == 0) {
            throw new NoMoreFreeDominoException();
        }
        int tileNumber = generator.nextInt(freeDominoSet.size());
        DominoTile dominoTile = freeDominoSet.remove(tileNumber);

        TileButton newTileButton = new TileButton(dominoTile);
        playerDominoButtons.add(newTileButton);
        newTileButton.addActionListener(this);
        add(newTileButton);
    }

    private void setDominoPosition() {

        for (int i = 0; i < playerDominoButtons.size(); i++) {
            playerDominoButtons.get(i).setBounds(new Rectangle((widthDomino + spaceDomino) * i + xDominoStart, yPlayerDomino, widthDomino, heightDomino));
            playerDominoButtons.get(i).getDominoTile().setPosition((widthDomino + spaceDomino) * i + xDominoStart, yPlayerDomino);
        }

        for (int i = 0; i < computerDominoSet.size(); i++) {
            computerDominoSet.get(i).setPosition((widthDomino + spaceDomino) * i + xDominoStart, yComputerDomino);
        }
    }

    public void refreshDominoPositions() {
        for (int i = 0; i < playerDominoButtons.size(); i++) {
            playerDominoButtons.get(i).setBounds(new Rectangle((widthDomino + spaceDomino) * i + xDominoStart, yPlayerDomino, widthDomino, heightDomino));
            playerDominoButtons.get(i).getDominoTile().setPosition((widthDomino + spaceDomino) * i + xDominoStart, yPlayerDomino);
        }

        for (int i = 0; i < computerDominoSet.size(); i++) {
            computerDominoSet.get(i).setPosition((widthDomino + spaceDomino) * i + xDominoStart, yComputerDomino);
        }

        int xOnBoard = xDominoStart;
        for (int i = 0; i < onBoardDomino.size(); i++) {
            if (onBoardDomino.get(i).isDouble())
                onBoardDomino.get(i).setPosition(xOnBoard, yDominoOnBoard - 20);
            else
                onBoardDomino.get(i).setPosition(xOnBoard, yDominoOnBoard);

            if (onBoardDomino.get(i).isDouble()) {
                xOnBoard += 41;
            } else {
                xOnBoard += 81;
            }
        }
    }

    public boolean move(DominoTile dominoTile) throws AdditionOnTwoSidesException {
        boolean movePossible = false;

        if (!onBoardDomino.isEmpty()) {
            DominoTile first = onBoardDomino.getFirst();
            DominoTile last = onBoardDomino.getLast();

            boolean moveOnFirst = false;
            if (first.getNumber1() == dominoTile.getNumber1()) {
                movePossible = true;
                moveOnFirst = true;
            } else if (first.getNumber1() == dominoTile.getNumber2()) {
                movePossible = true;
                moveOnFirst = true;
            }
            if (last.getNumber2() == dominoTile.getNumber1()) {
                if (movePossible) {
                    throw new AdditionOnTwoSidesException();
                }
                movePossible = true;
            } else if (last.getNumber2() == dominoTile.getNumber2()) {
                if (movePossible) {
                    throw new AdditionOnTwoSidesException();
                }
                movePossible = true;
            }
            if (movePossible) {

                if (moveOnFirst) {
                    addToFirst(dominoTile);
                } else {
                    addToLast(dominoTile);
                }

                return true;
            } else {
                return false;
            }
        } else {
            if (dominoTile.isDouble())
                dominoTile.loadVerticalImage();

            onBoardDomino.addFirst(dominoTile);
            return true;
        }
    }

    private void addToFirst(DominoTile dominoTile) {
        if (dominoTile.isDouble())
            dominoTile.loadVerticalImage();

        if (onBoardDomino.getFirst().getNumber1() == dominoTile.getNumber2()) {
            onBoardDomino.addFirst(dominoTile);
        } else {
            dominoTile.flip();
            onBoardDomino.addFirst(dominoTile);
        }
    }

    private void addToLast(DominoTile dominoTile) {
        if (dominoTile.isDouble())
            dominoTile.loadVerticalImage();

        if (onBoardDomino.getLast().getNumber2() == dominoTile.getNumber1()) {
            onBoardDomino.addLast(dominoTile);
        } else {
            dominoTile.flip();
            onBoardDomino.addLast(dominoTile);
        }
    }

    private void computerMove() throws NoMoreMoveException {

        boolean moved = false;
        DominoTile dominoTile = computerDominoSet.get(0);
        int i = 0;
        try {
            while (!moved && i < computerDominoSet.size()) {
                dominoTile = computerDominoSet.get(i);
                moved = move(dominoTile);
                i++;
            }
        } catch (AdditionOnTwoSidesException additionOnTwoSidesException) {
            addToFirst(dominoTile);
            moved = true;
        }
        if (moved) {
            if (dominoTile.isDouble())
                dominoTile.loadVerticalImage();

            computerDominoSet.remove(dominoTile);
            dominoTile.flipDominoFace(false);
            refreshDominoPositions();
            repaint();
            LOG.info("Computer moved " + dominoTile.toString());
        } else {
            if (freeDominoSet.size() == 0) {
                throw new NoMoreMoveException();
            }
            int tileNumber = generator.nextInt(freeDominoSet.size());
            DominoTile newComputerDomino = freeDominoSet.remove(tileNumber);
            computerDominoSet.add(newComputerDomino);
            newComputerDomino.flipDominoFace(false);

            refreshDominoPositions();
            repaint();

            LOG.info("No more possible move for computer");

            computerMove();
        }
    }

    private boolean canPlayerMove() {
        boolean movePossible = false;

        for (int i = 0; i < playerDominoButtons.size(); i++) {
            DominoTile dominoTile = playerDominoButtons.get(i).getDominoTile();

            if (!onBoardDomino.isEmpty()) {
                DominoTile first = onBoardDomino.getFirst();
                DominoTile last = onBoardDomino.getLast();
                if (first.getNumber1() == dominoTile.getNumber1()) {
                    movePossible = true;
                } else if (first.getNumber1() == dominoTile.getNumber2()) {
                    movePossible = true;
                }
                if (last.getNumber2() == dominoTile.getNumber1()) {
                    movePossible = true;
                } else if (last.getNumber2() == dominoTile.getNumber2()) {
                    movePossible = true;
                }
                if (movePossible) {
                    return true;
                }
            } else {
                return true;
            }

        }
        return false;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (DominoTile tile : computerDominoSet) {
            g.drawImage(tile.getTileImage(), tile.getX(), tile.getY(), this); // see javadoc for more info on the parameters
        }
        for (DominoTile tile : onBoardDomino) {
            g.drawImage(tile.getTileImage(), tile.getX(), tile.getY(), this); // see javadoc for more info on the parameters
        }
        for (DominoTile tile : freeDominoSet) {
            g.drawImage(tile.getTileImage(), -100, -100, this);
        }
    }

    public void load() {

        for (DominoTile dominoTile : computerDominoSet) {
            dominoTile.flipDominoFace(false);
            dominoTile.flipDominoFace(false);
        }

        for (TileButton button : playerDominoButtons) {
            button.addActionListener(this);
            add(button);
        }

        for (DominoTile dominoTile : onBoardDomino) {
            if (dominoTile.isDouble()) {
                dominoTile.loadVerticalImage();
            }
        }

        refreshDominoPositions();
        repaint();

    }

    public void actionPerformed(ActionEvent e) {
        move((TileButton) e.getSource());

    }

    private void move(TileButton obj) {

        DominoTile dominoTile = obj.getDominoTile();

        try {
            boolean playerMoved = false;
            try {
                playerMoved = move(dominoTile);
            } catch (AdditionOnTwoSidesException e1) {
                Object stringArray[] = {language.getOptionPanelLeft(), language.getOptionPanelRight()};
                int response = JOptionPane.showOptionDialog(this.getParent(), language.getOptionPanelMessage(), language.getOptionPanel(),
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray,
                        stringArray[0]);

                switch (response) {
                    case JOptionPane.YES_OPTION:
                        addToFirst(dominoTile);
                        playerMoved = true;
                        break;
                    case JOptionPane.NO_OPTION:
                        addToLast(dominoTile);
                        playerMoved = true;
                        break;
                    case JOptionPane.CLOSED_OPTION:
                        playerMoved = false;
                        break;
                }
            }

            if (playerMoved) {

                LOG.info("Player moved" + dominoTile.toString());
                playerDominoButtons.remove(obj);
                Container parent = obj.getParent();     //remove button from player domino set
                if (parent != null) {
                    parent.remove(obj);
                    parent.repaint();//remove button from panel
                }

                refreshDominoPositions();


                addExtraPointsTo(true);
                if (playerDominoButtons.size() == 0) {
                    isWin = true;
                    throw new GameOverException();
                }

                try {
                    computerMove();
                    refreshDominoPositions();
                    addExtraPointsTo(false);
                } catch (NoMoreMoveException e1) {
                    setPoints();
                    throw new GameOverException();
                }

                if (computerDominoSet.size() == 0) {
                    throw new GameOverException();
                }

                while (!canPlayerMove()) {
                    LOG.info("No more possible move for player");
                    showNoMoreMoveMessage();
                }
            } else {
                LOG.info("Player can't move tile " + dominoTile.toString());
            }
        } catch (GameOverException gameOverExc) {
            for (TileButton button : playerDominoButtons) {
                button.removeActionListener(this);
            }
            for (DominoTile tile : computerDominoSet) {
                tile.flipDominoFace(true);
            }
            LOG.info("Game over");
            setPoints();
            showGameOverMessages();
        }
    }

    private void showNoMoreMoveMessage() throws GameOverException {
        JOptionPane.showMessageDialog(this.getParent(), language.getNoMoreMoveMessage(),
                language.getNoMoreMove(), JOptionPane.INFORMATION_MESSAGE);
        try {
            addDominoPlayer();
        } catch (NoMoreFreeDominoException e1) {
            setPoints();
            if (isWin) {
                LOG.info("Player Win");
                throw new GameOverException(language.getGameOverWinMessage());
            } else {
                LOG.info("Player Lose");
                throw new GameOverException(language.getGameOverLoseMessage());
            }
        }
        refreshDominoPositions();
    }

    private void showGameOverMessages() {
        String gameOverMessage = null;

        playerName = JOptionPane.showInputDialog(this.getParent(),
                language.getInputPlayerNameMessage(),
                language.getInputPlayerName(),
                JOptionPane.QUESTION_MESSAGE);
        if (playerName == null) {
            playerName = "AAA";
        }

        dataBaseConnector.saveScore(playerName, computerPoints, playerPoints, isWin);

        if (isWin) {
            gameOverMessage = language.getGameOverWinMessage();
        } else {
            gameOverMessage = language.getGameOverLoseMessage();
        }

        JOptionPane.showMessageDialog(this.getParent(),
                gameOverMessage +
                        "\n " + language.getPlayerName() + playerName +
                        "\n " + language.getGameOverPlayerScore() + playerPoints +
                        "\n " + language.getGameOverComputerScore() + computerPoints,
                language.getGameOver(), JOptionPane.INFORMATION_MESSAGE);
    }

    private void addExtraPointsTo(boolean player) {
        int sum = 0;
        if (onBoardDomino.getFirst().getNumber1() == onBoardDomino.getFirst().getNumber2()) {
            sum += onBoardDomino.getFirst().getNumber1() * 2;
        } else {
            sum += onBoardDomino.getFirst().getNumber1();
        }
        if (onBoardDomino.getLast().getNumber1() == onBoardDomino.getLast().getNumber2()) {
            sum += onBoardDomino.getLast().getNumber2() * 2;
        } else {
            sum += onBoardDomino.getLast().getNumber2();
        }

        if (sum % 5 == 0) {
            if (player) {
                playerExtraPoints += sum;
            } else {
                computerExtraPoints += sum;
            }
        }

    }

    private void setPoints() {
        if (computerDominoSet.size() > 0 && playerDominoButtons.size() > 0) {
            if (sumPlayerDomino() < sumComputerDomino()) {
                isWin = true;
                playerPoints = sumComputerDomino() - sumPlayerDomino();
                playerPoints += playerExtraPoints;
                playerPoints = roundTo5(playerPoints);
            } else {
                computerPoints = sumPlayerDomino() - sumComputerDomino();
                computerPoints = roundTo5(computerPoints);
                computerPoints += computerExtraPoints;
            }
        } else {
            if (isWin) {
                playerPoints = sumComputerDomino();
                playerPoints += playerExtraPoints;
                playerPoints = roundTo5(playerPoints);
            } else {
                computerPoints = sumPlayerDomino();
                computerPoints = roundTo5(computerPoints);
                computerPoints += computerExtraPoints;
            }
        }
    }

    private int sumPlayerDomino() {
        int sum = 0;
        for (TileButton tile : playerDominoButtons) {
            sum += tile.getDominoTile().getNumber1() + tile.getDominoTile().getNumber2();
        }
        return sum;
    }

    private int sumComputerDomino() {
        int sum = 0;
        for (DominoTile tile : computerDominoSet) {
            sum += tile.getNumber1() + tile.getNumber2();
        }
        return sum;
    }

    private int roundTo5(int num) {
        int i = num;
        if (i % 5 >= 3) {
            i += 5 - i % 5;
        } else {
            i -= i % 5;
        }
        if (i < 0) {
            i = 0;
        }
        return i;
    }

    public ArrayList<DominoTile> getComputerDominoSet() {
        return computerDominoSet;
    }

    public void setComputerDominoSet(ArrayList<DominoTile> computerDominoSet) {
        this.computerDominoSet = computerDominoSet;
    }

    public ArrayList<DominoTile> getFreeDominoSet() {
        return freeDominoSet;
    }

    public void setFreeDominoSet(ArrayList<DominoTile> freeDominoSet) {
        this.freeDominoSet = freeDominoSet;
    }

    public ArrayList<TileButton> getPlayerDominoButtons() {
        return playerDominoButtons;
    }

    public void setPlayerDominoButtons(ArrayList<TileButton> playerDominoButtons) {
        this.playerDominoButtons = playerDominoButtons;
    }

    public LinkedList<DominoTile> getOnBoardDomino() {
        return onBoardDomino;
    }

    public void setOnBoardDomino(LinkedList<DominoTile> onBoardDomino) {
        this.onBoardDomino = onBoardDomino;
    }
}
