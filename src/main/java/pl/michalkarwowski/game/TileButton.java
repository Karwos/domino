package pl.michalkarwowski.game;

import javax.swing.*;

public class TileButton extends JButton {
    private DominoTile dominoTile;

    public TileButton(DominoTile dominoTile){
        super(new ImageIcon(dominoTile.getTileImage()));

        this.dominoTile = dominoTile;
    }

    public DominoTile getDominoTile() {
        return dominoTile;
    }

    public void setDominoTile(DominoTile dominoTile) {
        this.dominoTile = dominoTile;
    }

    @Override
    public String toString() {
        return dominoTile.toString();
    }
}
