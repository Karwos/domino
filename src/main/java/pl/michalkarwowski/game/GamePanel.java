package pl.michalkarwowski.game;

import pl.michalkarwowski.GameState;
import pl.michalkarwowski.connector.DataBaseConnector;
import pl.michalkarwowski.language.Language;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class GamePanel extends JPanel {

    private Board board;
    GameState gameState;

    public GamePanel(Language language, String user, DataBaseConnector dataBaseConnector) {
        super(new BorderLayout());

        board = new Board(language, user, dataBaseConnector);
        board.setPreferredSize(new Dimension(2000, 400));
        JScrollPane scrollPane = new JScrollPane(board);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        add(scrollPane, BorderLayout.CENTER);
    }

    public void newGame() {
        board.newGame();
    }

    public void saveGame() {

        gameState = new GameState();

        gameState.setComputerDominoSet( deepCopy(board.getComputerDominoSet()));
        gameState.setFreeDominoSet(deepCopy(board.getFreeDominoSet()));
        LinkedList<DominoTile> onBoard = new LinkedList<DominoTile>();
        for (DominoTile dominoTile : board.getOnBoardDomino()){
            onBoard.add(new DominoTile(dominoTile.getNumber1(),dominoTile.getNumber2(),dominoTile.isFaceUp()));
        }

        gameState.setOnBoardDomino(onBoard);

        ArrayList<TileButton> newlist = new ArrayList<TileButton>();
        for (TileButton tileButton : board.getPlayerDominoButtons()){
            newlist.add(new TileButton(new DominoTile( tileButton.getDominoTile().getNumber1(),
                    tileButton.getDominoTile().getNumber2(),
                    true)));
        }
        gameState.setPlayerDominoButtons(newlist);



    }

    private ArrayList<DominoTile> deepCopy(ArrayList<DominoTile> list){
        ArrayList<DominoTile> newlist = new ArrayList<DominoTile>();
        for (DominoTile dominoTile : list){
            newlist.add(new DominoTile(dominoTile.getNumber1(),dominoTile.getNumber2(),dominoTile.isFaceUp()));
        }
        return newlist;
    }

    public void loadGame() {
        for(TileButton tileButton: gameState.getPlayerDominoButtons()){
            Container parent = tileButton.getParent();     //remove button from player domino set
            if(parent != null) {
                parent.remove(tileButton);
                parent.repaint();
            }
        }
        for(TileButton tileButton: board.getPlayerDominoButtons()){
            Container parent = tileButton.getParent();     //remove button from player domino set
            if(parent != null) {
                parent.remove(tileButton);
                parent.repaint();
            }
        }
        ArrayList<TileButton> newlist = new ArrayList<TileButton>();
        for (TileButton tileButton : gameState.getPlayerDominoButtons()){
            newlist.add(new TileButton(new DominoTile( tileButton.getDominoTile().getNumber1(),
                    tileButton.getDominoTile().getNumber2(),
            false)));
        }
        board.setPlayerDominoButtons(newlist);
        board.setComputerDominoSet(deepCopy(gameState.getComputerDominoSet()));
        board.setFreeDominoSet(deepCopy(gameState.getFreeDominoSet()));
        LinkedList<DominoTile> onBoard = new LinkedList<DominoTile>();
        for (DominoTile dominoTile : gameState.getOnBoardDomino()){
            onBoard.add(new DominoTile(dominoTile.getNumber1(),dominoTile.getNumber2(),dominoTile.isFaceUp()));
        }
        board.setOnBoardDomino(onBoard);

        for(TileButton tileButton: board.getPlayerDominoButtons()){
            Container parent = tileButton.getParent();     //remove button from player domino set
            if(parent != null) {
                parent.remove(tileButton);
                parent.repaint();
            }
        }
        board.load();
    }

}
