package pl.michalkarwowski;

import pl.michalkarwowski.game.DominoTile;
import pl.michalkarwowski.game.TileButton;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.LinkedList;

public class GameState {


    private ArrayList<DominoTile> computerDominoSet;   //computers domino tiles
    private ArrayList<DominoTile> freeDominoSet;       //free domino tiles on board
    private ArrayList<TileButton> playerDominoButtons;
    private LinkedList<DominoTile> onBoardDomino;      //domino on board

    public ArrayList<DominoTile> getComputerDominoSet() {
        return computerDominoSet;
    }

    public void setComputerDominoSet(ArrayList<DominoTile> computerDominoSet) {
        if (this.computerDominoSet != null && !this.computerDominoSet.isEmpty()) {
            this.computerDominoSet.clear();
        }
        this.computerDominoSet = computerDominoSet;
    }

    public ArrayList<DominoTile> getFreeDominoSet() {
        return freeDominoSet;
    }

    public void setFreeDominoSet(ArrayList<DominoTile> freeDominoSet) {
        this.freeDominoSet = freeDominoSet;
    }

    public ArrayList<TileButton> getPlayerDominoButtons() {
        return playerDominoButtons;
    }

    public void setPlayerDominoButtons(ArrayList<TileButton> playerDominoButtons) {
        this.playerDominoButtons = playerDominoButtons;
    }

    public LinkedList<DominoTile> getOnBoardDomino() {
        return onBoardDomino;
    }

    public void setOnBoardDomino(LinkedList<DominoTile> onBoardDomino) {
        this.onBoardDomino = onBoardDomino;
    }
}
