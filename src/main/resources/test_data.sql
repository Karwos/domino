USE domino;

DROP TABLE IF EXISTS games, users;

#	CREATE TABLE `users` (
#	  `name` varchar(255) NOT NULL,
#	  `city` varchar(255) NOT NULL,
#	  `password` varchar(255) NOT NULL,
#	  PRIMARY KEY (`name`)
#	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO Users(Name, Password, City) VALUES('Jack', 'jack123', 'Warsaw');
INSERT INTO Users(Name, Password, City) VALUES('MadMax', 'qazasd', 'London');
    


# CREATE TABLE `games` (
#  `gameId` int(11) NOT NULL,
#  `userName` varchar(255) NOT NULL,
#  `computerScore` int(11) NOT NULL,
#  `playerScore` int(11) NOT NULL,
#  `isWin` tinyint(4) NOT NULL,
#  PRIMARY KEY (`gameId`),
#  UNIQUE KEY `gameId_UNIQUE` (`gameId`),
#  KEY `userName_idx` (`userName`),
#  CONSTRAINT `userName` FOREIGN KEY (`userName`) REFERENCES `users` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into Games(game_Id, user_Name, computer_Score, player_Score, is_Win) values (1, 'Jack', 45,0, false);
insert into Games(game_Id, user_Name, computer_Score, player_Score, is_Win) values (2, 'Jack', 0,12, true);
insert into Games(game_Id, user_Name, computer_Score, player_Score, is_Win) values (3, 'MadMax', 0,25, true);
insert into Games(game_Id, user_Name, computer_Score, player_Score, is_Win) values (4, 'MadMax', 15,0, false);

select *
from users U
inner join Games G on g.user_Name = U.Name
order by player_Score desc